import { all, takeEvery } from 'redux-saga/effects';
import { fetchUsers } from './Users/UserActions';

export default function* rootSaga() {
    // put all sagas here
    yield all([
        takeEvery('FETCH_USERS', fetchUsers)
    ])
}