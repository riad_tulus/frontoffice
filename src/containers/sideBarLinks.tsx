import DashboardIcon from '@material-ui/icons/Dashboard';
import UserIcon from '@material-ui/icons/People';
import * as React from 'react';
import Dashboard from "../containers/Dashboard/Dashboard";
import User from "../containers/Users/User";

export interface ISideBarItems {
    component: any;
    icon: any;
    label: string
    path: string;
}

export const sideBarItems: ISideBarItems[] = [
    {
      component: Dashboard,
      icon: <DashboardIcon />,
      label: 'Dashboard',
      path: '/dashboard'
    },
    {
      component: User,
      icon: <UserIcon />,
      label: 'Users',
      path: '/users'
    }
];
