import Hidden from '@material-ui/core/Hidden/Hidden';
import * as React from 'react';
import { Header, SideBar } from '../../components';
import getRoutes from '../../routes';
import { sideBarItems } from '../sideBarLinks';
import './App.css';

class App extends React.Component {
    public render(): any {
        return (
            <div className="root">
                <Header />
                <Hidden smDown={true}>
                    <SideBar links={sideBarItems}/>
                </Hidden>
                <main className="content">
                    {getRoutes()}
                </main>
            </div>
        );
    }
}


export default App;

