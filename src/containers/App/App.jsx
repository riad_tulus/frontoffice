// import Hidden from '@material-ui/core/Hidden/Hidden';
import * as React from 'react';
import { Header, SideBar } from '../../components';
import getRoutes from '../../routes';
import { sideBarItems } from '../sideBarLinks';
import './App.css';

class App extends React.Component {

    state={
        open:false
    }

    openDrawer = () => {
          this.setState({open:true})
    } 

    closeDrawer = () => {
        this.setState({open:false})
    } 

    render() {
        return (
            <div className="root">
                <Header openDrawer={this.openDrawer} />
                    <SideBar open={this.state.open} links={sideBarItems} onClose={this.closeDrawer}/>
                <main className="content">
                    {getRoutes()}
                </main>
            </div>
        );
    }
}


export default App;

