import { call, put } from 'redux-saga/effects';

export function* fetchUsers() {
    yield put({type: 'FETCH_USER_START', fetching: true})
    try {
        const response = yield call(fetch, 'https://jsonplaceholder.typicode.com/users')
        const data = yield call(response.json.bind(response))
        yield put({type: 'FETCH_USER_SUCCESS', fetching: false, payload: data})
    } catch (error) {
        yield put({type: 'FETCH_USER_ERROR', fetching: false, error})
    }

}