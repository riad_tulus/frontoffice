import { Checkbox, Grid, LinearProgress, Paper, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import * as React from 'react';
import { connect } from 'react-redux';
import './User.css';

export interface IUserProp {
    users: any[];
    fetching: boolean;
    dispatch: any;
}

export interface IStateProp {
    users: any[];
    fetching: boolean;
}


class User extends React.Component<IUserProp> {

    constructor(props: IUserProp) {
        super(props)
    }

    public componentWillMount () {
        this.props.dispatch({type: 'FETCH_USERS'})
    }

    public render () {
        const users = this.props.users;
        const progressLine = this.props.fetching ? <LinearProgress variant="query" /> : null;

        return (
            <div>
                {progressLine}
                {!this.props.fetching ? <Grid container={true} direction="row">
                    <Grid item={true} xs={true}>
                        <Paper className="table-paper">
                            <Table>
                                <TableHead>
                                <TableRow>
                                    <TableCell padding="checkbox">
                                        <Checkbox checked={false} />
                                    </TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell numeric={true}>Phone</TableCell>
                                    <TableCell>Website</TableCell>
                                    <TableCell>Campany name</TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                {users.map((user: any) => {
                                    return (
                                    <TableRow key={user.id}>
                                        <TableCell padding="checkbox">
                                            <Checkbox checked={false} />
                                        </TableCell>
                                        <TableCell component="th" scope="row">
                                            {user.name}
                                        </TableCell>
                                        <TableCell>{user.email}</TableCell>
                                        <TableCell numeric={true}>{user.phone}</TableCell>
                                        <TableCell>{user.website}</TableCell>
                                        <TableCell>{user.company.name}</TableCell>
                                    </TableRow>
                                    );
                                })}
                                </TableBody>
                            </Table>
                        </Paper>
                    </Grid>
                </Grid> : null }
            </div>
        );
    }
}

export default connect((state: any) : IStateProp => {
    return {
        fetching: state.UserReducer.fetching,
        users: state.UserReducer.users
    }
})(User)