const defaultState = {
    error: null,
    fetching: false,
    users: [],
};

export default function reducer(state=defaultState, action : any) {

    switch (action.type) {
        case "FETCH_USER_START": {
            return {...state, fetching: true};
        }
        case "FETCH_USER_ERROR": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_USER_SUCCESS": {
            return {...state, fetching: false, users: action.payload}
        }
    }
    return {...state}
}