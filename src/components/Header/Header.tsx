import { Avatar, Grid } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Hidden from '@material-ui/core/Hidden/Hidden';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import * as React from 'react';
import './Header.css';

export interface IHeaderState {
    anchorEl: any
}

class Header extends React.Component<{}, IHeaderState> {

    constructor (props: any) {
        super(props)

        this.state = {
            anchorEl: null,
        };
    }

    public handleClick = () => (event: React.FormEvent<HTMLElement>) => {
        this.setState({anchorEl: event.currentTarget})
    }

    public handleCloseProfileMenu = () => () => {
        this.setState({anchorEl: null})
    }

    public render () {

        const {anchorEl} = this.state

        return (
            <AppBar>
                <Toolbar>
                    <Grid container={true}>
                        <Grid item={true} xs={6}>
                            <Hidden mdUp={true}>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer">
                                    <MenuIcon />
                                </IconButton>
                            </Hidden>
                        </Grid>
                        <Hidden smDown={true}>
                            <Grid container={true} justify="flex-end" direction="row">
                                <IconButton onClick={this.handleClick()}>
                                    <Avatar color="inherit">H</Avatar>
                                </IconButton>
                                <Menu
                                    id="profile-menu"
                                    anchorEl={anchorEl}
                                    open={Boolean(anchorEl)}
                                    PaperProps={
                                        {
                                            style: {
                                                marginTop: 40,
                                                width: 180
                                            }
                                        }
                                    }
                                    onClose={this.handleCloseProfileMenu()}>
                                    <MenuItem>
                                        Profile
                                    </MenuItem>
                                    <MenuItem>
                                        Logout
                                    </MenuItem>
                                </Menu>
                            </Grid>
                        </Hidden>
                    </Grid>
                </Toolbar>
            </AppBar>
        );
    }
}

export default Header;