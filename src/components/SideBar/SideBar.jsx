import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { ISideBarItems } from '../../containers/sideBarLinks';
import './SideBar.css';


export default class SideBar extends React.Component {

    render () {
        return (
            <Drawer
                classes={{paper: "drawerPaper"}}
                open={this.props.open}
                onClose={this.props.onClose}
                anchor="left">
                <Toolbar>
                    {/* Logo */}
                </Toolbar>
                <List>
                    {this.props.links.map((item, index) => {
                        return (
                            <Link key={index} className="router-link" to={item.path}>
                                <ListItem button={true}>
                                    <ListItemIcon>
                                        {item.icon}
                                    </ListItemIcon>
                                    <ListItemText primary={item.label} />
                                </ListItem>
                            </Link>
                        )
                    })}
                </List>
            </Drawer>
        )
    }
}