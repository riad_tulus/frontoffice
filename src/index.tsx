import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import App from './containers/App/App';
import { createStore, runSaga } from './redux';
import registerServiceWorker from './registerServiceWorker';


const store = createStore();

ReactDOM.render(
    <Provider store={store} key="provider">
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

runSaga();
registerServiceWorker();
