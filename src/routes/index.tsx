import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { sideBarItems } from '../containers/sideBarLinks';


export default function getRoutes(): any {
    return (
        <Switch>
            {sideBarItems.map(
                (item, index) => {
                    return <Route key={index} path={item.path} component={item.component} />
                }
            )}
        </Switch>
    );
};