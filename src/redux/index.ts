import { applyMiddleware, combineReducers, createStore as _createStore } from 'redux';
import createSagaMidlleware from 'redux-saga';
import containersReducers from '../containers/reducers';
import rootSaga from '../containers/sagas'

const sagaMidlleware = createSagaMidlleware();

export function runSaga(): any {
    sagaMidlleware.run(rootSaga)
}

export function createStore() : any {
    const middleware = applyMiddleware(...[sagaMidlleware]);
    const reducer = combineReducers({...containersReducers});
    return _createStore(reducer, middleware);
}